#include "ControlCardMB.h"
#include "stdafx.h"
#include <QSerialPort>
#include <QSerialPortInfo>


int		ControlCardMB::objectCount = 0;


ControlCardMB::ControlCardMB()
{





}

ControlCardMB::ControlCardMB(QString port, int baudRate)
{
	
	ui.setupUi(this);
	parseCounter = 0;
	isOpen = false;

	id = objectCount;
	objectCount++;
	ReadFile();
	SetDisplay();

	serialControl = new QSerialPort();
	//
	serialControl->setPortName(port);
	serialControl->setBaudRate(baudRate);
	serialControl->setDataBits(QSerialPort::DataBits(8));
	serialControl->setParity(QSerialPort::Parity(0));
	serialControl->setStopBits(QSerialPort::StopBits(1));
	serialControl->setFlowControl(QSerialPort::FlowControl(0));


	connect(serialControl, SIGNAL(readyRead()), this, SLOT(ReadData()));
	connect(ui.spinBoxCH1, SIGNAL(valueChanged(int)), ui.horizontalSliderCH1, SLOT(setValue(int)));
	connect(ui.horizontalSliderCH1, SIGNAL(valueChanged(int)), ui.spinBoxCH1, SLOT(setValue(int)));
	connect(ui.spinBoxCH1, SIGNAL(valueChanged(int)), this, SLOT(ChangeData()));
//	connect(ui.horizontalSliderCH1, SIGNAL(valueChanged(int)), this, SLOT(ChangeData()));
	//ui.spinBoxCH1->valueChanged()
	//ui.spinBoxCH1->valueChanged
	if (serialControl->open(QIODevice::ReadWrite))
	{
		isOpen = true;
		serialControl->write("hello\n");
	}



}


void ControlCardMB::SetDisplay()
{
	QGroupBox *groupBox = new QGroupBox(tr("Exclusive Radio Buttons"));
		QVBoxLayout *vbox = new QVBoxLayout; 

		QRadioButton *radio1 = new QRadioButton(tr("&Radio button 1"));
		QRadioButton *radio2 = new QRadioButton(tr("R&adio button 2"));
		QRadioButton *radio3 = new QRadioButton(tr("Ra&dio button 3"));
		
		for(int i = 0; i < 10; i ++)
		inputButtons[i] = new QCheckBox;

		inputButtons[0]->setCheckable(true);
		inputButtons[0]->setChecked(true);
		
		vbox->addWidget(radio1);
		vbox->addWidget(radio2);
		for (int i = 0; i < 10; i++)
		{
			vbox->addWidget(inputButtons[i]);
		}
		vbox->addStretch(1);
		ui.inputBox1->setLayout(vbox);
		ui.inputBox1->setMaximumHeight(300);
		ui.InputBox0->setMaximumHeight(300);
		ui.CommonInputBox->setMaximumHeight(320);

/*	inputButtons->setCheckable(false);
	inputButtons->setChecked(true);

	inputLayout->addWidget(inputButtons);
	ui.InputBox0->setLayout(inputLayout);
	*/

}

ControlCardMB::~ControlCardMB()
{
}





void ControlCardMB::ReadData()
{

	QByteArray tmp;
	QByteArray tmpNum;
	char portNum;
	int inNum = 0;
	int numCounter = 0;
	int test = 0;

	if (isOpen)
	{
		getBuffer = serialControl->readAll();

		for (int i = 0; i < getBuffer.size(); i++)
		{

			parseBuffer[parseCounter] = getBuffer[i];


			if ((parseBuffer[parseCounter] == '\n'))
			{
				for ( int j = 0; j < parseCounter; j++)
				{
					tmp[j] = parseBuffer[j];
					if (j > 1)
					{
						tmpNum[numCounter] = parseBuffer[j];
						numCounter++;
					}
				}
				numCounter = 0;
				

				switch (tmp[0])
				{
				case ('I'):
				{ 
					inNum = tmpNum.toInt();
					inNum = atoi(tmpNum);
					if (tmp[1]=='0')
					getInput(0, inNum);
					else
					getInput(1, inNum);

				}
				case ('O'):
				{

				}

				

				}

				
				parseCounter = 0;

			}
			else
			{
				parseCounter++;
			}
		}


	}
	//serialControl->write(getBuffer);

}

void ControlCardMB::WriteData(QByteArray data)
{
	if (serialControl->open(QIODevice::ReadWrite))
	{
		serialControl->write(data);
	}



}

void ControlCardMB::ChangeData()
{
	int vred;

}

void ControlCardMB::getInput(int port, UCHAR byte)
{
	{
		
		for (int i = 0; i < 8; i++)
		{
			if ((byte & input[port][i].address) == input[port][i].address)
			{
				input[port][i].prevValue = input[port][i].value;
				input[port][i].value = 1;
			}
			else
			{
				input[port][i].prevValue = input[port][i].value;
				input[port][i].value = 0;
			}
		}
	}


}

int ControlCardMB::ReadFile(void)
{
	QString filePath;
	QStringList values;
	QVariant var;
	 


	//filePath = "D:\\Git_MBvision\\MBsoftware32bit\\MBsoftware32Bit\\Win32\\Reference\\signali\\ControlCardMB0.ini";
	filePath = QApplication::applicationDirPath() + QString("/../%1/signali/ControlCardMB%2.ini").arg(REF_FOLDER_NAME).arg(id);
	QSettings settings(filePath, QSettings::IniFormat);

	int adresses[16] = { 0x01, 0x02, 0x04, 0x08, 0x10, 0x20, 0x40, 0x80};
	char regName[2] = { 'A', 'B' };
	//inputs
	for (int j = 0; j < 2; j++)
	{
		for (int i = 0; i < 8; i++)
		{
			values = settings.value(QString("Input%1_%2").arg(regName[j]).arg(i)).toStringList();

			if (values.size() > 0)
			{
				input[j][i].SetSignal(values[0], adresses[i], 1);
				values.clear();
			}
			else
			{
				input[j][i].SetSignal(QString("S%1").arg(i), adresses[i], 0);


				settings.setValue(QString("input%1").arg(i), QString("input%1").arg(i));
				settings.sync();
			}
		}
	}

	//outputs
	for (int j = 0; j < 2; j++)
	{
		for (int i = 0; i < 8; i++)
		{
			values = settings.value(QString("Output%1_%2").arg(regName[j]).arg(i)).toStringList();

			if (values.size() > 0)
			{
				output[j][i].SetSignal(values[0], adresses[i], 1);
				values.clear();
			}
			else
			{
				output[j][i].SetSignal(QString("O%1").arg(i), adresses[i], 0);
				settings.setValue(QString("output%1").arg(i), QString("output%1").arg(i));
				settings.sync();

			}
		}
	}

	return 0;
}

