/********************************************************************************
** Form generated from reading UI file 'imageProcessing.ui'
**
** Created by: Qt User Interface Compiler version 5.11.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_IMAGEPROCESSING_H
#define UI_IMAGEPROCESSING_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QGraphicsView>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QTabWidget>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_imageProcessing
{
public:
    QGridLayout *gridLayout;
    QGridLayout *gridLayout_3;
    QLabel *label_3;
    QSpacerItem *horizontalSpacer_3;
    QLabel *labelLocation;
    QPushButton *buttonImageDown;
    QPushButton *buttonCamDown;
    QLabel *label_2;
    QPushButton *pushButton_5;
    QLabel *label;
    QPushButton *buttonCamUp;
    QPushButton *pushButton_6;
    QPushButton *buttonImageUp;
    QLabel *label_4;
    QGraphicsView *imageView;
    QSpacerItem *horizontalSpacer;
    QGridLayout *gridLayout_2;
    QTabWidget *tabWidget;
    QWidget *tab;
    QWidget *tab_2;

    void setupUi(QWidget *imageProcessing)
    {
        if (imageProcessing->objectName().isEmpty())
            imageProcessing->setObjectName(QStringLiteral("imageProcessing"));
        imageProcessing->resize(1131, 932);
        gridLayout = new QGridLayout(imageProcessing);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        gridLayout_3 = new QGridLayout();
        gridLayout_3->setObjectName(QStringLiteral("gridLayout_3"));
        label_3 = new QLabel(imageProcessing);
        label_3->setObjectName(QStringLiteral("label_3"));
        label_3->setMinimumSize(QSize(50, 0));
        label_3->setAlignment(Qt::AlignBottom|Qt::AlignLeading|Qt::AlignLeft);

        gridLayout_3->addWidget(label_3, 3, 11, 1, 1);

        horizontalSpacer_3 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_3->addItem(horizontalSpacer_3, 3, 6, 1, 1);

        labelLocation = new QLabel(imageProcessing);
        labelLocation->setObjectName(QStringLiteral("labelLocation"));
        labelLocation->setMinimumSize(QSize(100, 0));
        QFont font;
        font.setPointSize(18);
        labelLocation->setFont(font);
        labelLocation->setAlignment(Qt::AlignBottom|Qt::AlignLeading|Qt::AlignLeft);

        gridLayout_3->addWidget(labelLocation, 3, 5, 1, 1);

        buttonImageDown = new QPushButton(imageProcessing);
        buttonImageDown->setObjectName(QStringLiteral("buttonImageDown"));
        buttonImageDown->setMinimumSize(QSize(50, 50));
        buttonImageDown->setMaximumSize(QSize(50, 50));

        gridLayout_3->addWidget(buttonImageDown, 3, 3, 1, 1);

        buttonCamDown = new QPushButton(imageProcessing);
        buttonCamDown->setObjectName(QStringLiteral("buttonCamDown"));
        buttonCamDown->setMinimumSize(QSize(50, 50));
        buttonCamDown->setMaximumSize(QSize(50, 50));

        gridLayout_3->addWidget(buttonCamDown, 3, 1, 1, 1);

        label_2 = new QLabel(imageProcessing);
        label_2->setObjectName(QStringLiteral("label_2"));
        label_2->setMinimumSize(QSize(50, 0));
        label_2->setAlignment(Qt::AlignBottom|Qt::AlignLeading|Qt::AlignLeft);

        gridLayout_3->addWidget(label_2, 3, 13, 1, 1);

        pushButton_5 = new QPushButton(imageProcessing);
        pushButton_5->setObjectName(QStringLiteral("pushButton_5"));
        pushButton_5->setMinimumSize(QSize(60, 50));
        pushButton_5->setMaximumSize(QSize(60, 50));

        gridLayout_3->addWidget(pushButton_5, 3, 9, 1, 1);

        label = new QLabel(imageProcessing);
        label->setObjectName(QStringLiteral("label"));
        label->setMinimumSize(QSize(50, 0));
        label->setAlignment(Qt::AlignBottom|Qt::AlignLeading|Qt::AlignLeft);

        gridLayout_3->addWidget(label, 3, 12, 1, 1);

        buttonCamUp = new QPushButton(imageProcessing);
        buttonCamUp->setObjectName(QStringLiteral("buttonCamUp"));
        buttonCamUp->setMinimumSize(QSize(50, 50));
        buttonCamUp->setMaximumSize(QSize(50, 50));

        gridLayout_3->addWidget(buttonCamUp, 3, 4, 1, 1);

        pushButton_6 = new QPushButton(imageProcessing);
        pushButton_6->setObjectName(QStringLiteral("pushButton_6"));
        pushButton_6->setMinimumSize(QSize(60, 50));
        pushButton_6->setMaximumSize(QSize(50, 50));

        gridLayout_3->addWidget(pushButton_6, 3, 7, 1, 1);

        buttonImageUp = new QPushButton(imageProcessing);
        buttonImageUp->setObjectName(QStringLiteral("buttonImageUp"));
        buttonImageUp->setMinimumSize(QSize(50, 50));
        buttonImageUp->setMaximumSize(QSize(50, 50));

        gridLayout_3->addWidget(buttonImageUp, 1, 3, 1, 1);

        label_4 = new QLabel(imageProcessing);
        label_4->setObjectName(QStringLiteral("label_4"));
        label_4->setMinimumSize(QSize(50, 0));
        label_4->setAlignment(Qt::AlignBottom|Qt::AlignLeading|Qt::AlignLeft);

        gridLayout_3->addWidget(label_4, 3, 10, 1, 1);


        gridLayout->addLayout(gridLayout_3, 2, 1, 1, 1);

        imageView = new QGraphicsView(imageProcessing);
        imageView->setObjectName(QStringLiteral("imageView"));
        imageView->viewport()->setProperty("cursor", QVariant(QCursor(Qt::CrossCursor)));
        imageView->setMouseTracking(true);
        imageView->setFrameShape(QFrame::WinPanel);

        gridLayout->addWidget(imageView, 4, 1, 1, 2);

        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Minimum, QSizePolicy::Minimum);

        gridLayout->addItem(horizontalSpacer, 2, 0, 1, 1);

        gridLayout_2 = new QGridLayout();
        gridLayout_2->setObjectName(QStringLiteral("gridLayout_2"));

        gridLayout->addLayout(gridLayout_2, 4, 0, 1, 1);

        tabWidget = new QTabWidget(imageProcessing);
        tabWidget->setObjectName(QStringLiteral("tabWidget"));
        tabWidget->setMaximumSize(QSize(16777215, 120));
        tab = new QWidget();
        tab->setObjectName(QStringLiteral("tab"));
        tabWidget->addTab(tab, QString());
        tab_2 = new QWidget();
        tab_2->setObjectName(QStringLiteral("tab_2"));
        tabWidget->addTab(tab_2, QString());

        gridLayout->addWidget(tabWidget, 0, 0, 1, 3);


        retranslateUi(imageProcessing);

        tabWidget->setCurrentIndex(0);


        QMetaObject::connectSlotsByName(imageProcessing);
    } // setupUi

    void retranslateUi(QWidget *imageProcessing)
    {
        imageProcessing->setWindowTitle(QApplication::translate("imageProcessing", "ImageProcessing", nullptr));
        label_3->setText(QApplication::translate("imageProcessing", "TextLabel", nullptr));
        labelLocation->setText(QApplication::translate("imageProcessing", "TextLabel", nullptr));
        buttonImageDown->setText(QApplication::translate("imageProcessing", "IMAGE-", nullptr));
        buttonCamDown->setText(QApplication::translate("imageProcessing", "CAM-", nullptr));
        label_2->setText(QApplication::translate("imageProcessing", "TextLabel", nullptr));
        pushButton_5->setText(QApplication::translate("imageProcessing", "ZOOM OUT", nullptr));
        label->setText(QApplication::translate("imageProcessing", "TextLabel", nullptr));
        buttonCamUp->setText(QApplication::translate("imageProcessing", "CAM+", nullptr));
        pushButton_6->setText(QApplication::translate("imageProcessing", "ZOOM IN", nullptr));
        buttonImageUp->setText(QApplication::translate("imageProcessing", "IMAGE+", nullptr));
        label_4->setText(QApplication::translate("imageProcessing", "TextLabel", nullptr));
        tabWidget->setTabText(tabWidget->indexOf(tab), QApplication::translate("imageProcessing", "Tab 1", nullptr));
        tabWidget->setTabText(tabWidget->indexOf(tab_2), QApplication::translate("imageProcessing", "Tab 2", nullptr));
    } // retranslateUi

};

namespace Ui {
    class imageProcessing: public Ui_imageProcessing {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_IMAGEPROCESSING_H
