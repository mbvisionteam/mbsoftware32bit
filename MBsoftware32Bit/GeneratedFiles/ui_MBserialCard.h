/********************************************************************************
** Form generated from reading UI file 'MBserialCard.ui'
**
** Created by: Qt User Interface Compiler version 5.11.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MBSERIALCARD_H
#define UI_MBSERIALCARD_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QSlider>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_ControlCard
{
public:
    QGridLayout *gridLayout;
    QGroupBox *CommonInputBox;
    QHBoxLayout *horizontalLayout_2;
    QGroupBox *InputBox0;
    QVBoxLayout *verticalLayout_3;
    QGroupBox *inputBox1;
    QGroupBox *groupBox_4;
    QGroupBox *OutputBox;
    QHBoxLayout *horizontalLayout;
    QGroupBox *outA;
    QVBoxLayout *verticalLayout;
    QCheckBox *outA0;
    QCheckBox *outA1;
    QCheckBox *outA2;
    QCheckBox *outA3;
    QCheckBox *outA4;
    QCheckBox *outA5;
    QCheckBox *outA6;
    QCheckBox *outA7;
    QGroupBox *outB;
    QVBoxLayout *verticalLayout_2;
    QCheckBox *outB0;
    QCheckBox *outB1;
    QCheckBox *outB2;
    QCheckBox *outB3;
    QCheckBox *outB4;
    QCheckBox *outB5;
    QCheckBox *outB6;
    QCheckBox *outB7;
    QGroupBox *groupBox;
    QGridLayout *gridLayout_2;
    QCheckBox *checkBox_33;
    QComboBox *comboBox;
    QSlider *horizontalSliderCH1;
    QSpinBox *spinBoxCH1;
    QLabel *label;
    QGroupBox *groupBox_3;
    QGroupBox *groupBox_2;
    QGroupBox *groupBox_5;

    void setupUi(QWidget *ControlCard)
    {
        if (ControlCard->objectName().isEmpty())
            ControlCard->setObjectName(QStringLiteral("ControlCard"));
        ControlCard->resize(1019, 758);
        gridLayout = new QGridLayout(ControlCard);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        CommonInputBox = new QGroupBox(ControlCard);
        CommonInputBox->setObjectName(QStringLiteral("CommonInputBox"));
        CommonInputBox->setFlat(true);
        horizontalLayout_2 = new QHBoxLayout(CommonInputBox);
        horizontalLayout_2->setObjectName(QStringLiteral("horizontalLayout_2"));
        InputBox0 = new QGroupBox(CommonInputBox);
        InputBox0->setObjectName(QStringLiteral("InputBox0"));
        verticalLayout_3 = new QVBoxLayout(InputBox0);
        verticalLayout_3->setObjectName(QStringLiteral("verticalLayout_3"));

        horizontalLayout_2->addWidget(InputBox0);

        inputBox1 = new QGroupBox(CommonInputBox);
        inputBox1->setObjectName(QStringLiteral("inputBox1"));

        horizontalLayout_2->addWidget(inputBox1);


        gridLayout->addWidget(CommonInputBox, 3, 2, 1, 2);

        groupBox_4 = new QGroupBox(ControlCard);
        groupBox_4->setObjectName(QStringLiteral("groupBox_4"));
        groupBox_4->setMaximumSize(QSize(500, 500));

        gridLayout->addWidget(groupBox_4, 2, 3, 1, 1);

        OutputBox = new QGroupBox(ControlCard);
        OutputBox->setObjectName(QStringLiteral("OutputBox"));
        OutputBox->setMaximumSize(QSize(600, 300));
        OutputBox->setFlat(true);
        OutputBox->setCheckable(false);
        horizontalLayout = new QHBoxLayout(OutputBox);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        outA = new QGroupBox(OutputBox);
        outA->setObjectName(QStringLiteral("outA"));
        verticalLayout = new QVBoxLayout(outA);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        outA0 = new QCheckBox(outA);
        outA0->setObjectName(QStringLiteral("outA0"));

        verticalLayout->addWidget(outA0);

        outA1 = new QCheckBox(outA);
        outA1->setObjectName(QStringLiteral("outA1"));

        verticalLayout->addWidget(outA1);

        outA2 = new QCheckBox(outA);
        outA2->setObjectName(QStringLiteral("outA2"));

        verticalLayout->addWidget(outA2);

        outA3 = new QCheckBox(outA);
        outA3->setObjectName(QStringLiteral("outA3"));

        verticalLayout->addWidget(outA3);

        outA4 = new QCheckBox(outA);
        outA4->setObjectName(QStringLiteral("outA4"));

        verticalLayout->addWidget(outA4);

        outA5 = new QCheckBox(outA);
        outA5->setObjectName(QStringLiteral("outA5"));

        verticalLayout->addWidget(outA5);

        outA6 = new QCheckBox(outA);
        outA6->setObjectName(QStringLiteral("outA6"));

        verticalLayout->addWidget(outA6);

        outA7 = new QCheckBox(outA);
        outA7->setObjectName(QStringLiteral("outA7"));

        verticalLayout->addWidget(outA7);


        horizontalLayout->addWidget(outA);

        outB = new QGroupBox(OutputBox);
        outB->setObjectName(QStringLiteral("outB"));
        verticalLayout_2 = new QVBoxLayout(outB);
        verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
        outB0 = new QCheckBox(outB);
        outB0->setObjectName(QStringLiteral("outB0"));

        verticalLayout_2->addWidget(outB0);

        outB1 = new QCheckBox(outB);
        outB1->setObjectName(QStringLiteral("outB1"));

        verticalLayout_2->addWidget(outB1);

        outB2 = new QCheckBox(outB);
        outB2->setObjectName(QStringLiteral("outB2"));

        verticalLayout_2->addWidget(outB2);

        outB3 = new QCheckBox(outB);
        outB3->setObjectName(QStringLiteral("outB3"));

        verticalLayout_2->addWidget(outB3);

        outB4 = new QCheckBox(outB);
        outB4->setObjectName(QStringLiteral("outB4"));

        verticalLayout_2->addWidget(outB4);

        outB5 = new QCheckBox(outB);
        outB5->setObjectName(QStringLiteral("outB5"));

        verticalLayout_2->addWidget(outB5);

        outB6 = new QCheckBox(outB);
        outB6->setObjectName(QStringLiteral("outB6"));

        verticalLayout_2->addWidget(outB6);

        outB7 = new QCheckBox(outB);
        outB7->setObjectName(QStringLiteral("outB7"));

        verticalLayout_2->addWidget(outB7);


        horizontalLayout->addWidget(outB);


        gridLayout->addWidget(OutputBox, 3, 0, 1, 2);

        groupBox = new QGroupBox(ControlCard);
        groupBox->setObjectName(QStringLiteral("groupBox"));
        gridLayout_2 = new QGridLayout(groupBox);
        gridLayout_2->setObjectName(QStringLiteral("gridLayout_2"));
        checkBox_33 = new QCheckBox(groupBox);
        checkBox_33->setObjectName(QStringLiteral("checkBox_33"));

        gridLayout_2->addWidget(checkBox_33, 3, 0, 1, 1);

        comboBox = new QComboBox(groupBox);
        comboBox->addItem(QString());
        comboBox->addItem(QString());
        comboBox->addItem(QString());
        comboBox->addItem(QString());
        comboBox->addItem(QString());
        comboBox->setObjectName(QStringLiteral("comboBox"));

        gridLayout_2->addWidget(comboBox, 2, 2, 1, 1);

        horizontalSliderCH1 = new QSlider(groupBox);
        horizontalSliderCH1->setObjectName(QStringLiteral("horizontalSliderCH1"));
        horizontalSliderCH1->setMaximum(4000);
        horizontalSliderCH1->setOrientation(Qt::Horizontal);
        horizontalSliderCH1->setInvertedAppearance(false);
        horizontalSliderCH1->setInvertedControls(false);

        gridLayout_2->addWidget(horizontalSliderCH1, 1, 0, 1, 2);

        spinBoxCH1 = new QSpinBox(groupBox);
        spinBoxCH1->setObjectName(QStringLiteral("spinBoxCH1"));
        spinBoxCH1->setMaximum(4000);

        gridLayout_2->addWidget(spinBoxCH1, 1, 2, 1, 1);

        label = new QLabel(groupBox);
        label->setObjectName(QStringLiteral("label"));

        gridLayout_2->addWidget(label, 2, 0, 1, 1);


        gridLayout->addWidget(groupBox, 2, 0, 1, 1);

        groupBox_3 = new QGroupBox(ControlCard);
        groupBox_3->setObjectName(QStringLiteral("groupBox_3"));

        gridLayout->addWidget(groupBox_3, 2, 2, 1, 1);

        groupBox_2 = new QGroupBox(ControlCard);
        groupBox_2->setObjectName(QStringLiteral("groupBox_2"));

        gridLayout->addWidget(groupBox_2, 2, 1, 1, 1);

        groupBox_5 = new QGroupBox(ControlCard);
        groupBox_5->setObjectName(QStringLiteral("groupBox_5"));
        groupBox_5->setMaximumSize(QSize(16777215, 100));

        gridLayout->addWidget(groupBox_5, 1, 0, 1, 4);


        retranslateUi(ControlCard);

        QMetaObject::connectSlotsByName(ControlCard);
    } // setupUi

    void retranslateUi(QWidget *ControlCard)
    {
        ControlCard->setWindowTitle(QApplication::translate("ControlCard", "Form", nullptr));
        CommonInputBox->setTitle(QApplication::translate("ControlCard", "Input", nullptr));
        InputBox0->setTitle(QApplication::translate("ControlCard", "Input 0-7", nullptr));
        inputBox1->setTitle(QApplication::translate("ControlCard", "Input 8-15", nullptr));
        groupBox_4->setTitle(QApplication::translate("ControlCard", "Channel 4", nullptr));
        OutputBox->setTitle(QApplication::translate("ControlCard", "Output", nullptr));
        outA->setTitle(QApplication::translate("ControlCard", "Output 0-7", nullptr));
        outA0->setText(QApplication::translate("ControlCard", "CheckBox", nullptr));
        outA1->setText(QApplication::translate("ControlCard", "CheckBox", nullptr));
        outA2->setText(QApplication::translate("ControlCard", "CheckBox", nullptr));
        outA3->setText(QApplication::translate("ControlCard", "CheckBox", nullptr));
        outA4->setText(QApplication::translate("ControlCard", "CheckBox", nullptr));
        outA5->setText(QApplication::translate("ControlCard", "CheckBox", nullptr));
        outA6->setText(QApplication::translate("ControlCard", "CheckBox", nullptr));
        outA7->setText(QApplication::translate("ControlCard", "CheckBox", nullptr));
        outB->setTitle(QApplication::translate("ControlCard", "Output 8-15", nullptr));
        outB0->setText(QApplication::translate("ControlCard", "CheckBox", nullptr));
        outB1->setText(QApplication::translate("ControlCard", "CheckBox", nullptr));
        outB2->setText(QApplication::translate("ControlCard", "CheckBox", nullptr));
        outB3->setText(QApplication::translate("ControlCard", "CheckBox", nullptr));
        outB4->setText(QApplication::translate("ControlCard", "CheckBox", nullptr));
        outB5->setText(QApplication::translate("ControlCard", "CheckBox", nullptr));
        outB6->setText(QApplication::translate("ControlCard", "CheckBox", nullptr));
        outB7->setText(QApplication::translate("ControlCard", "CheckBox", nullptr));
        groupBox->setTitle(QApplication::translate("ControlCard", "Channel1 ", nullptr));
        checkBox_33->setText(QApplication::translate("ControlCard", "CheckBox", nullptr));
        comboBox->setItemText(0, QApplication::translate("ControlCard", "2x", nullptr));
        comboBox->setItemText(1, QApplication::translate("ControlCard", "2.5x", nullptr));
        comboBox->setItemText(2, QApplication::translate("ControlCard", "3x", nullptr));
        comboBox->setItemText(3, QApplication::translate("ControlCard", "3.5x", nullptr));
        comboBox->setItemText(4, QApplication::translate("ControlCard", "4x", nullptr));

        label->setText(QApplication::translate("ControlCard", "Oja\304\215anje:", nullptr));
        groupBox_3->setTitle(QApplication::translate("ControlCard", "Channel 3", nullptr));
        groupBox_2->setTitle(QApplication::translate("ControlCard", "Channel 2", nullptr));
        groupBox_5->setTitle(QApplication::translate("ControlCard", "GroupBox", nullptr));
    } // retranslateUi

};

namespace Ui {
    class ControlCard: public Ui_ControlCard {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MBSERIALCARD_H
