/********************************************************************************
** Form generated from reading UI file 'MBsoftware32Bit.ui'
**
** Created by: Qt User Interface Compiler version 5.11.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MBSOFTWARE32BIT_H
#define UI_MBSOFTWARE32BIT_H

#include <QtCore/QVariant>
#include <QtGui/QIcon>
#include <QtWidgets/QApplication>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QTabWidget>
#include <QtWidgets/QToolButton>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MBsoftware32BitClass
{
public:
    QWidget *centralWidget;
    QGridLayout *gridLayout;
    QTabWidget *MainMenuBar;
    QWidget *tabTypes;
    QWidget *tabCameras;
    QWidget *tabStatistics;
    QWidget *tabSignals;
    QWidget *tabImageProcessing;
    QHBoxLayout *horizontalLayout;
    QGroupBox *groupBox;
    QHBoxLayout *horizontalLayout_2;
    QToolButton *buttonImageProcessing;
    QSpacerItem *horizontalSpacer;
    QSpacerItem *horizontalSpacer_2;
    QSpacerItem *verticalSpacer;

    void setupUi(QMainWindow *MBsoftware32BitClass)
    {
        if (MBsoftware32BitClass->objectName().isEmpty())
            MBsoftware32BitClass->setObjectName(QStringLiteral("MBsoftware32BitClass"));
        MBsoftware32BitClass->setWindowModality(Qt::NonModal);
        MBsoftware32BitClass->resize(1024, 640);
        QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(MBsoftware32BitClass->sizePolicy().hasHeightForWidth());
        MBsoftware32BitClass->setSizePolicy(sizePolicy);
        MBsoftware32BitClass->setMinimumSize(QSize(0, 0));
        MBsoftware32BitClass->setMaximumSize(QSize(16777215, 16777215));
        MBsoftware32BitClass->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
        centralWidget = new QWidget(MBsoftware32BitClass);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        QSizePolicy sizePolicy1(QSizePolicy::Minimum, QSizePolicy::Minimum);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(centralWidget->sizePolicy().hasHeightForWidth());
        centralWidget->setSizePolicy(sizePolicy1);
        centralWidget->setMinimumSize(QSize(1024, 640));
        centralWidget->setMaximumSize(QSize(1920, 1300));
        gridLayout = new QGridLayout(centralWidget);
        gridLayout->setSpacing(6);
        gridLayout->setContentsMargins(11, 11, 11, 11);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        MainMenuBar = new QTabWidget(centralWidget);
        MainMenuBar->setObjectName(QStringLiteral("MainMenuBar"));
        sizePolicy.setHeightForWidth(MainMenuBar->sizePolicy().hasHeightForWidth());
        MainMenuBar->setSizePolicy(sizePolicy);
        MainMenuBar->setMinimumSize(QSize(800, 120));
        MainMenuBar->setMaximumSize(QSize(16777215, 150));
        QFont font;
        font.setPointSize(12);
        MainMenuBar->setFont(font);
        MainMenuBar->setTabPosition(QTabWidget::North);
        MainMenuBar->setTabShape(QTabWidget::Triangular);
        MainMenuBar->setMovable(true);
        MainMenuBar->setTabBarAutoHide(true);
        tabTypes = new QWidget();
        tabTypes->setObjectName(QStringLiteral("tabTypes"));
        MainMenuBar->addTab(tabTypes, QString());
        tabCameras = new QWidget();
        tabCameras->setObjectName(QStringLiteral("tabCameras"));
        MainMenuBar->addTab(tabCameras, QString());
        tabStatistics = new QWidget();
        tabStatistics->setObjectName(QStringLiteral("tabStatistics"));
        MainMenuBar->addTab(tabStatistics, QString());
        tabSignals = new QWidget();
        tabSignals->setObjectName(QStringLiteral("tabSignals"));
        MainMenuBar->addTab(tabSignals, QString());
        tabImageProcessing = new QWidget();
        tabImageProcessing->setObjectName(QStringLiteral("tabImageProcessing"));
        tabImageProcessing->setAcceptDrops(false);
        horizontalLayout = new QHBoxLayout(tabImageProcessing);
        horizontalLayout->setSpacing(6);
        horizontalLayout->setContentsMargins(11, 11, 11, 11);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        groupBox = new QGroupBox(tabImageProcessing);
        groupBox->setObjectName(QStringLiteral("groupBox"));
        QFont font1;
        font1.setPointSize(10);
        groupBox->setFont(font1);
        groupBox->setAutoFillBackground(false);
        groupBox->setTitle(QStringLiteral(""));
        horizontalLayout_2 = new QHBoxLayout(groupBox);
        horizontalLayout_2->setSpacing(6);
        horizontalLayout_2->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_2->setObjectName(QStringLiteral("horizontalLayout_2"));
        buttonImageProcessing = new QToolButton(groupBox);
        buttonImageProcessing->setObjectName(QStringLiteral("buttonImageProcessing"));
        buttonImageProcessing->setMinimumSize(QSize(60, 60));
        QIcon icon;
        icon.addFile(QStringLiteral(":/MBsoftware32Bit/res/imageProcessing.png"), QSize(), QIcon::Normal, QIcon::Off);
        buttonImageProcessing->setIcon(icon);
        buttonImageProcessing->setIconSize(QSize(49, 48));
        buttonImageProcessing->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
        buttonImageProcessing->setAutoRaise(false);

        horizontalLayout_2->addWidget(buttonImageProcessing);

        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer);


        horizontalLayout->addWidget(groupBox);

        MainMenuBar->addTab(tabImageProcessing, QString());

        gridLayout->addWidget(MainMenuBar, 0, 1, 1, 1);

        horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout->addItem(horizontalSpacer_2, 0, 2, 1, 1);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout->addItem(verticalSpacer, 1, 1, 1, 1);

        MBsoftware32BitClass->setCentralWidget(centralWidget);

        retranslateUi(MBsoftware32BitClass);

        MainMenuBar->setCurrentIndex(4);


        QMetaObject::connectSlotsByName(MBsoftware32BitClass);
    } // setupUi

    void retranslateUi(QMainWindow *MBsoftware32BitClass)
    {
        MBsoftware32BitClass->setWindowTitle(QApplication::translate("MBsoftware32BitClass", "MBsoftware32Bit", nullptr));
        MainMenuBar->setTabText(MainMenuBar->indexOf(tabTypes), QApplication::translate("MBsoftware32BitClass", "Types", nullptr));
        MainMenuBar->setTabText(MainMenuBar->indexOf(tabCameras), QApplication::translate("MBsoftware32BitClass", "Cameras", nullptr));
        MainMenuBar->setTabText(MainMenuBar->indexOf(tabStatistics), QApplication::translate("MBsoftware32BitClass", "Statistics", nullptr));
        MainMenuBar->setTabText(MainMenuBar->indexOf(tabSignals), QApplication::translate("MBsoftware32BitClass", "I/O signals", nullptr));
        buttonImageProcessing->setText(QApplication::translate("MBsoftware32BitClass", "ImageProcessing Dialog", nullptr));
        MainMenuBar->setTabText(MainMenuBar->indexOf(tabImageProcessing), QApplication::translate("MBsoftware32BitClass", "ImageProcessing", nullptr));
    } // retranslateUi

};

namespace Ui {
    class MBsoftware32BitClass: public Ui_MBsoftware32BitClass {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MBSOFTWARE32BIT_H
