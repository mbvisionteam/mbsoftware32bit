/********************************************************************************
** Form generated from reading UI file 'Camera.ui'
**
** Created by: Qt User Interface Compiler version 5.11.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_CAMERA_H
#define UI_CAMERA_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QGraphicsView>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSlider>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_Camera
{
public:
    QGridLayout *gridLayout;
    QGroupBox *groupBox;
    QGridLayout *gridLayout_2;
    QLabel *lableR;
    QLabel *labelG;
    QLabel *labelB;
    QLabel *labelGain;
    QSlider *sliderGain;
    QSpacerItem *horizontalSpacer_2;
    QSlider *sliderExpo;
    QLabel *labelImageSize;
    QSpinBox *spinGain;
    QSpacerItem *horizontalSpacer_4;
    QLabel *labelCameraName;
    QLabel *labelExpo;
    QSpinBox *spinExpo;
    QPushButton *buttonLive;
    QSpacerItem *horizontalSpacer_3;
    QSpacerItem *verticalSpacer_2;
    QSpacerItem *verticalSpacer_3;
    QSpacerItem *verticalSpacer;
    QGraphicsView *imageView;

    void setupUi(QWidget *Camera)
    {
        if (Camera->objectName().isEmpty())
            Camera->setObjectName(QStringLiteral("Camera"));
        Camera->resize(1290, 786);
        QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(Camera->sizePolicy().hasHeightForWidth());
        Camera->setSizePolicy(sizePolicy);
        Camera->setMinimumSize(QSize(0, 0));
        Camera->setMaximumSize(QSize(16777215, 16777215));
        gridLayout = new QGridLayout(Camera);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        groupBox = new QGroupBox(Camera);
        groupBox->setObjectName(QStringLiteral("groupBox"));
        QSizePolicy sizePolicy1(QSizePolicy::Fixed, QSizePolicy::Fixed);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(groupBox->sizePolicy().hasHeightForWidth());
        groupBox->setSizePolicy(sizePolicy1);
        groupBox->setMinimumSize(QSize(640, 150));
        groupBox->setMaximumSize(QSize(1920, 1080));
        groupBox->setCursor(QCursor(Qt::ArrowCursor));
        groupBox->setMouseTracking(true);
        groupBox->setTabletTracking(true);
        gridLayout_2 = new QGridLayout(groupBox);
        gridLayout_2->setObjectName(QStringLiteral("gridLayout_2"));
        lableR = new QLabel(groupBox);
        lableR->setObjectName(QStringLiteral("lableR"));
        lableR->setMinimumSize(QSize(80, 0));
        lableR->setMaximumSize(QSize(200, 16777215));

        gridLayout_2->addWidget(lableR, 3, 7, 1, 1);

        labelG = new QLabel(groupBox);
        labelG->setObjectName(QStringLiteral("labelG"));
        labelG->setMinimumSize(QSize(80, 0));
        labelG->setMaximumSize(QSize(200, 16777215));

        gridLayout_2->addWidget(labelG, 3, 8, 1, 1);

        labelB = new QLabel(groupBox);
        labelB->setObjectName(QStringLiteral("labelB"));
        labelB->setMinimumSize(QSize(80, 0));
        labelB->setMaximumSize(QSize(200, 16777215));

        gridLayout_2->addWidget(labelB, 3, 9, 1, 1);

        labelGain = new QLabel(groupBox);
        labelGain->setObjectName(QStringLiteral("labelGain"));

        gridLayout_2->addWidget(labelGain, 0, 5, 1, 1);

        sliderGain = new QSlider(groupBox);
        sliderGain->setObjectName(QStringLiteral("sliderGain"));
        sizePolicy1.setHeightForWidth(sliderGain->sizePolicy().hasHeightForWidth());
        sliderGain->setSizePolicy(sizePolicy1);
        sliderGain->setMinimumSize(QSize(400, 0));
        sliderGain->setMaximumSize(QSize(250, 30));
        sliderGain->setOrientation(Qt::Horizontal);

        gridLayout_2->addWidget(sliderGain, 0, 6, 1, 1);

        horizontalSpacer_2 = new QSpacerItem(77, 37, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_2->addItem(horizontalSpacer_2, 0, 9, 1, 1);

        sliderExpo = new QSlider(groupBox);
        sliderExpo->setObjectName(QStringLiteral("sliderExpo"));
        sizePolicy1.setHeightForWidth(sliderExpo->sizePolicy().hasHeightForWidth());
        sliderExpo->setSizePolicy(sizePolicy1);
        sliderExpo->setMinimumSize(QSize(400, 30));
        sliderExpo->setMaximumSize(QSize(250, 30));
        sliderExpo->setOrientation(Qt::Horizontal);

        gridLayout_2->addWidget(sliderExpo, 1, 6, 1, 1);

        labelImageSize = new QLabel(groupBox);
        labelImageSize->setObjectName(QStringLiteral("labelImageSize"));

        gridLayout_2->addWidget(labelImageSize, 1, 0, 1, 1);

        spinGain = new QSpinBox(groupBox);
        spinGain->setObjectName(QStringLiteral("spinGain"));
        spinGain->setMaximumSize(QSize(50, 16777215));

        gridLayout_2->addWidget(spinGain, 0, 7, 1, 1);

        horizontalSpacer_4 = new QSpacerItem(37, 37, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_2->addItem(horizontalSpacer_4, 0, 3, 1, 1);

        labelCameraName = new QLabel(groupBox);
        labelCameraName->setObjectName(QStringLiteral("labelCameraName"));
        labelCameraName->setMinimumSize(QSize(200, 0));

        gridLayout_2->addWidget(labelCameraName, 0, 0, 1, 1);

        labelExpo = new QLabel(groupBox);
        labelExpo->setObjectName(QStringLiteral("labelExpo"));

        gridLayout_2->addWidget(labelExpo, 1, 5, 1, 1);

        spinExpo = new QSpinBox(groupBox);
        spinExpo->setObjectName(QStringLiteral("spinExpo"));
        spinExpo->setMaximumSize(QSize(50, 16777215));

        gridLayout_2->addWidget(spinExpo, 1, 7, 1, 1);

        buttonLive = new QPushButton(groupBox);
        buttonLive->setObjectName(QStringLiteral("buttonLive"));
        buttonLive->setMinimumSize(QSize(200, 40));
        buttonLive->setMaximumSize(QSize(100, 40));
        QFont font;
        font.setPointSize(20);
        buttonLive->setFont(font);
        buttonLive->setAutoFillBackground(true);
        buttonLive->setStyleSheet(QStringLiteral(""));
        buttonLive->setCheckable(false);
        buttonLive->setAutoDefault(false);
        buttonLive->setFlat(false);

        gridLayout_2->addWidget(buttonLive, 0, 2, 1, 1);

        horizontalSpacer_3 = new QSpacerItem(37, 37, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_2->addItem(horizontalSpacer_3, 0, 1, 1, 1);

        verticalSpacer_2 = new QSpacerItem(77, 37, QSizePolicy::Minimum, QSizePolicy::Fixed);

        gridLayout_2->addItem(verticalSpacer_2, 2, 8, 1, 1);


        gridLayout->addWidget(groupBox, 0, 2, 1, 1);

        verticalSpacer_3 = new QSpacerItem(1240, 13, QSizePolicy::Minimum, QSizePolicy::Fixed);

        gridLayout->addItem(verticalSpacer_3, 1, 2, 1, 1);

        verticalSpacer = new QSpacerItem(1240, 13, QSizePolicy::Minimum, QSizePolicy::Fixed);

        gridLayout->addItem(verticalSpacer, 3, 2, 1, 1);

        imageView = new QGraphicsView(Camera);
        imageView->setObjectName(QStringLiteral("imageView"));
        imageView->setEnabled(true);
        imageView->setMinimumSize(QSize(1240, 400));
        imageView->setMaximumSize(QSize(424222, 4244224));
        QFont font1;
        font1.setFamily(QStringLiteral("Comic Sans MS"));
        font1.setPointSize(20);
        imageView->setFont(font1);
        imageView->viewport()->setProperty("cursor", QVariant(QCursor(Qt::CrossCursor)));
        imageView->setMouseTracking(true);
        imageView->setTabletTracking(true);

        gridLayout->addWidget(imageView, 2, 2, 1, 1);


        retranslateUi(Camera);

        buttonLive->setDefault(false);


        QMetaObject::connectSlotsByName(Camera);
    } // setupUi

    void retranslateUi(QWidget *Camera)
    {
        Camera->setWindowTitle(QApplication::translate("Camera", "Form", nullptr));
        groupBox->setTitle(QApplication::translate("Camera", "GroupBox", nullptr));
        lableR->setText(QApplication::translate("Camera", "TextLabel", nullptr));
        labelG->setText(QApplication::translate("Camera", "TextLabel", nullptr));
        labelB->setText(QApplication::translate("Camera", "TextLabel", nullptr));
        labelGain->setText(QApplication::translate("Camera", "Gain:", nullptr));
        labelImageSize->setText(QApplication::translate("Camera", "TextLabel", nullptr));
        labelCameraName->setText(QApplication::translate("Camera", "TextLabel", nullptr));
        labelExpo->setText(QApplication::translate("Camera", "Expositure:", nullptr));
        buttonLive->setText(QApplication::translate("Camera", "EnableLive", nullptr));
    } // retranslateUi

};

namespace Ui {
    class Camera: public Ui_Camera {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_CAMERA_H
