/********************************************************************************
** Form generated from reading UI file 'LoginWindow.ui'
**
** Created by: Qt User Interface Compiler version 5.11.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_LOGINWINDOW_H
#define UI_LOGINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_LoginWindow
{
public:
    QLineEdit *lineEdit_prijava_geslo;
    QComboBox *comboBox_prijava;
    QPushButton *pushButton_prijava;
    QLabel *label_prijava_upor_ime;
    QLabel *label_prijava_geslo;

    void setupUi(QWidget *LoginWindow)
    {
        if (LoginWindow->objectName().isEmpty())
            LoginWindow->setObjectName(QStringLiteral("LoginWindow"));
        LoginWindow->setEnabled(true);
        LoginWindow->resize(316, 169);
        lineEdit_prijava_geslo = new QLineEdit(LoginWindow);
        lineEdit_prijava_geslo->setObjectName(QStringLiteral("lineEdit_prijava_geslo"));
        lineEdit_prijava_geslo->setEnabled(true);
        lineEdit_prijava_geslo->setGeometry(QRect(160, 80, 141, 31));
        lineEdit_prijava_geslo->setInputMethodHints(Qt::ImhHiddenText|Qt::ImhNoAutoUppercase|Qt::ImhNoPredictiveText|Qt::ImhSensitiveData);
        lineEdit_prijava_geslo->setInputMask(QStringLiteral(""));
        lineEdit_prijava_geslo->setEchoMode(QLineEdit::Password);
        comboBox_prijava = new QComboBox(LoginWindow);
        comboBox_prijava->addItem(QString());
        comboBox_prijava->addItem(QString());
        comboBox_prijava->addItem(QString());
        comboBox_prijava->setObjectName(QStringLiteral("comboBox_prijava"));
        comboBox_prijava->setGeometry(QRect(160, 20, 141, 31));
        pushButton_prijava = new QPushButton(LoginWindow);
        pushButton_prijava->setObjectName(QStringLiteral("pushButton_prijava"));
        pushButton_prijava->setGeometry(QRect(50, 140, 211, 23));
        pushButton_prijava->setStyleSheet(QStringLiteral(""));
        label_prijava_upor_ime = new QLabel(LoginWindow);
        label_prijava_upor_ime->setObjectName(QStringLiteral("label_prijava_upor_ime"));
        label_prijava_upor_ime->setGeometry(QRect(20, 30, 101, 16));
        QFont font;
        font.setFamily(QStringLiteral("MS Shell Dlg 2"));
        font.setPointSize(10);
        label_prijava_upor_ime->setFont(font);
        label_prijava_geslo = new QLabel(LoginWindow);
        label_prijava_geslo->setObjectName(QStringLiteral("label_prijava_geslo"));
        label_prijava_geslo->setGeometry(QRect(20, 90, 47, 13));
        QFont font1;
        font1.setPointSize(10);
        label_prijava_geslo->setFont(font1);

        retranslateUi(LoginWindow);

        QMetaObject::connectSlotsByName(LoginWindow);
    } // setupUi

    void retranslateUi(QWidget *LoginWindow)
    {
        LoginWindow->setWindowTitle(QApplication::translate("LoginWindow", "Login", nullptr));
        lineEdit_prijava_geslo->setText(QString());
        comboBox_prijava->setItemText(0, QApplication::translate("LoginWindow", "Administrator", nullptr));
        comboBox_prijava->setItemText(1, QApplication::translate("LoginWindow", "Operater", nullptr));
        comboBox_prijava->setItemText(2, QApplication::translate("LoginWindow", "Delavec", nullptr));

        pushButton_prijava->setText(QApplication::translate("LoginWindow", "Prijava", nullptr));
        label_prijava_upor_ime->setText(QApplication::translate("LoginWindow", "Uporabni\305\241ko ime", nullptr));
        label_prijava_geslo->setText(QApplication::translate("LoginWindow", "Geslo", nullptr));
    } // retranslateUi

};

namespace Ui {
    class LoginWindow: public Ui_LoginWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_LOGINWINDOW_H
