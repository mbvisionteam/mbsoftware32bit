/********************************************************************************
** Form generated from reading UI file 'LptControl.ui'
**
** Created by: Qt User Interface Compiler version 5.11.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_LPTCONTROL_H
#define UI_LPTCONTROL_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_LptControl
{
public:
    QGridLayout *gridLayout_2;

    void setupUi(QWidget *LptControl)
    {
        if (LptControl->objectName().isEmpty())
            LptControl->setObjectName(QStringLiteral("LptControl"));
        LptControl->resize(1207, 315);
        gridLayout_2 = new QGridLayout(LptControl);
        gridLayout_2->setObjectName(QStringLiteral("gridLayout_2"));

        retranslateUi(LptControl);

        QMetaObject::connectSlotsByName(LptControl);
    } // setupUi

    void retranslateUi(QWidget *LptControl)
    {
        LptControl->setWindowTitle(QApplication::translate("LptControl", "LPT control", nullptr));
    } // retranslateUi

};

namespace Ui {
    class LptControl: public Ui_LptControl {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_LPTCONTROL_H
