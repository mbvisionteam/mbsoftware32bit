/********************************************************************************
** Vsebuje knji�nice, ki jih uporablja ve�ina razredov v programu!
**
** Klicana je v .cpp datotekah in jo vedno vnesemo kot prvo z #include ukazom!
**
** Pospe�i prevajanje celotnega programa!
********************************************************************************/

#pragma once


#include "windows.h"
#pragma comment(lib, "ws2_32.lib")


#include <QtWidgets>


#include <QWidget>
#include <QGraphicsSceneMouseEvent>
//#include "qt_windows.h"
//#include <QtWidgets/QMainWindow>
//#include <QtWidgets/QApplication>
//#include <QWidget>
////#include <QTimer>
//#include <QFile>
//#include <QString>
//#include <QDebug>
//#include <QTextStream>
//#include <QDialog>


#include "opencv/cv.h"
#include "opencv2/core/core.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/calib3d/calib3d.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/videoio/videoio.hpp"



#define	TAB_BUTTTON_SIZE	80
#define TAB_ICON_SIZE		80
#define REF_FOLDER_NAME		"Reference"  //folder kjer shranjujemo nastavitve programa