#pragma once

#include "qt_windows.h"
#include "Tvichw32.h"
#include "ui_LptControl.h"
#include "Signals.h"
#include "stdafx.h"







class LptControl : public QWidget
{
	Q_OBJECT


public:
	static HANDLE		HwCtrl;	//only one instance of HwCtrl
	static bool			ActiveHW;
	static DWORD		nPort;
	static int			objectCount; //how many created objects
	UCHAR				portStatusValue;
	UCHAR				portDataValue;
	UCHAR				portControlValue;
	UCHAR				controlValue;


	Signals				output[8];
	Signals				input[5];
	Signals				control[4];

	//display
	QStandardItemModel	*poModelOutputs, *poModelInputs, *poModelControl;
	QStandardItem		*poListItemOutputs[8], *poListItemInputs[5], *poListItemControl[4];

	QGroupBox			*groupBox[3];
	QHBoxLayout		    *layout;
	QListView			*listViewOutputs, *listViewInputs, *listViewControl;
	QReadWriteLock		lockReadWrite;

	QTimer*				viewTimer;
private:

	USHORT				IRQ;

	ULONG				Flag_Intr;

	bool				ReadMode;

	DWORD				Sum_Ticks; //dodaj stevec v program
	DWORD				IRQCounter; //dodaj stevec v program
	DWORD				Flag_Tim; //dodaj stevec v program
	DWORD				OldTicker, CurrTicker; //dodaj stevec v program

	DWORD				portAddr;
	int					id;



	TOnHWInterrupt		HWHandler; //function which is called on interrupt


public:


	LptControl();
	LptControl(DWORD portAddress);
	~LptControl();


private:
	Ui::LptControl ui;


public:
	static bool OpenDriver(void); //only one instance of driver
	void Init(int port);  //set pins to false, set o/i, set port address
	unsigned char GetPortControlValue(void);
	void SetDataByte(UCHAR bits);
	void SetDataByte(UCHAR bits, bool value);
	void SetOutputValue(int outputNumber, bool value);
	unsigned char GetStatusValue(void);

	void SetControlByte(int contolBit, bool value);
	void SetControlByte(char bits);
	void SetControlByteAllON(void);
	void SetControlByteAllOFF(void);
	void SetDataBits(UCHAR bits, bool value);
	void SetDataBits(UCHAR bits);
	void UnSetDataBits(UCHAR bits);
	void SetOutputSignals();
	void SetInputSignals();
	void SetControlSignals();

	void ShowDialog();

	int ReadData(void);
	int WriteData(void);


	void CreateDisplay(void);



public slots:
	void SlotItemChanged(QStandardItem*);
	void SlotItemChangedControl(QStandardItem*);
	void TimerViewSignals();

};

