#pragma once
#include "stdafx.h"


using namespace cv;



class MemoryBuffer
{
public:
	MemoryBuffer();
	MemoryBuffer(int width, int height, int depth);
	~MemoryBuffer();

	cv::Mat* buffer;
	int width;
	int height;
	int depth;
	void Create(int width, int height, int depth);
};

