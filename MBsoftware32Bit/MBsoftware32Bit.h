#pragma once

#include <QTextEdit>
#include "LptControl.h"
#include "LoginWindow.h"
#include "ui_MBsoftware32Bit.h"
#include "ui_imageProcessing.h"
#include "Serial.h"
#include "ControlCardMB.h"
#include "Camera.h"
#include "ImagingSource.h"
#include "Timer.h"
#include "imageProcessing.h"

class MBsoftware32Bit : public QMainWindow
{
	Q_OBJECT

public:
	void closeEvent(QCloseEvent * event);
	MBsoftware32Bit(QWidget *parent = Q_NULLPTR);

	

//Deklaracija globalnih spremenljivk
public:
	QTimer * OnTimer;
	static std::vector<Camera*>			cam;
	static std::vector<MemoryBuffer*>   images;
	static std::vector<LptControl*>		lpt;
	static std::vector<Serial*>			serial;
	static std::vector<ControlCardMB*>	controlCardMB;
	static Timer							mmTimer;
	static std::vector<Timer*>				TestFun;
	static int								MBsoftware32Bit::mmCounter;


	imageProcessing* imageProcessingDlg;
private:
	Ui::MBsoftware32BitClass ui;

		

protected:
	static void __stdcall OnMultimediaTimer(UINT uTimerID, UINT, DWORD_PTR  dwUser, DWORD_PTR  dw1, DWORD_PTR  dw2);
	void keyPressEvent(QKeyEvent * event);
	void paintEvent(QPaintEvent *event) override;

public slots:
	void ViewTimer();
	void OnClickedShowLptButton(int);
	void OnClickedShowCamButton(int);
	void OnClickedShowImageProcessing(void);


	
	//izgled aplikacije
	void CreateCameraTab();
	void CreateSignalsTab();



private slots:
	void test();
	void OnFrameReady(int, int);


};
