#pragma once

#include <QWidget>
#include "ui_imageProcessing.h"
#include "Camera.h"

class imageProcessing : public QWidget
{
	Q_OBJECT

public:
	imageProcessing(QWidget *parent = Q_NULLPTR);
	//imageProcessing();
	~imageProcessing();


	static std::vector<Camera*>			processCam;
	static std::vector<MemoryBuffer*>	processImages;
	static	MemoryBuffer*						currImage;
	static	MemoryBuffer						DrawImage;
	float zoomFactor;
	int displayWindow;
	int displayImage;
	int prevDisplayWindow;
	int prevDisplayImage;
	bool isSceneAdded;

	QGraphicsPixmapItem*	pixmapVideo;

	void ConnectImages(std::vector<Camera*> inputCam, std::vector <MemoryBuffer*> inputImages);
	void ConnectImages(std::vector<Camera*> inputCam);
	void ReplaceCurrentBuffer();
	void test();




protected:
	bool eventFilter(QObject *obj, QEvent *event);
	void mousePressEvent(QMouseEvent *event);


private:
	Ui::imageProcessing ui;
	QGraphicsScene*	scene;


public slots:
	void OnPressedImageUp();
	void OnPressedImageDown();
	void OnPressedCamUp();
	void OnPressedCamDown();
	int ConvertImageForDisplay(int imageNumber);
	void ShowImages();
};
