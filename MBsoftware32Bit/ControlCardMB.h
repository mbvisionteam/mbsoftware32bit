#pragma once
#include <QSerialPort>
#include <QSerialPortInfo>
#include "ui_MBserialCard.h"

#include "qt_windows.h"

#include "Signals.h"


class ControlCardMB : public QWidget
{
	Q_OBJECT
public:
	ControlCardMB();
	ControlCardMB(QString port, int baudRate);
	void SetDisplay();
	~ControlCardMB();
	

private:
	Ui::ControlCard ui;
	
	QByteArray getBuffer;
	QByteArray parseBuffer;
	int parseCounter;
	static int			objectCount; //how many created objects
	QCheckBox *inputButtons[10];
	QHBoxLayout *inputLayout;
	

public:

	QSerialPort * serialControl;
	bool isOpen;
	Signals input[2][8];
	Signals output[2][8];
	int id;




	public slots:
	void ReadData();
	void WriteData(QByteArray data);
	void ChangeData();
	void getInput(int port, UCHAR byte);
	int ReadFile(void);
};

