#pragma once

#include "ui_LoginWindow.h"


class LoginWindow :
	public QWidget
{
	Q_OBJECT;

public:
	LoginWindow(QWidget *parent = 0);
	~LoginWindow();

private:
	Ui::LoginWindow ui;

public:
	void ShowDialog();
};

