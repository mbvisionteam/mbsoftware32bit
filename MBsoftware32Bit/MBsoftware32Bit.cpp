﻿#include "stdafx.h"
#include "MBsoftware32Bit.h"

//include za media timer ( multimedia timer )
#include<mmsystem.h>
#pragma comment(lib, "winmm.lib")

std::vector<Camera*>						MBsoftware32Bit::cam;
std::vector<MemoryBuffer*>					MBsoftware32Bit::images;
std::vector<LptControl*>					MBsoftware32Bit::lpt;
std::vector<Serial*>						MBsoftware32Bit::serial;
std::vector<ControlCardMB*>					MBsoftware32Bit::controlCardMB;
Timer										MBsoftware32Bit::mmTimer;
std::vector<Timer*>							MBsoftware32Bit::TestFun;
int											MBsoftware32Bit::mmCounter = 0;
int counter = 0;
//QSerialPort*					serial;
MMRESULT				FTimerID;

void MBsoftware32Bit::closeEvent(QCloseEvent *event)
{

	disconnect(this, 0, 0, 0);

	//first you have to close all timers
	timeKillEvent(FTimerID);
	timeEndPeriod(1);
	OnTimer->stop();


	for (int i = 0; i < cam.size(); i++)
	{
		delete (cam[i]);
	}
	
	for (int i = 0; i < images.size(); i++)
	{
		delete (images[i]);
	}

		for (int i = 0; i < lpt.size(); i++)
		{
			delete (lpt[i]);
		}
		lpt.clear();

		for (int i = 0; i < controlCardMB.size(); i++)
		{
			delete (controlCardMB[i]);
		}
		controlCardMB.clear();

		for (int i = 0; i < serial.size(); i++)
		{
			delete (serial[i]);
		}
		serial.clear();

		delete imageProcessingDlg;

}

MBsoftware32Bit::MBsoftware32Bit(QWidget *parent)
	: QMainWindow(parent)
{
	ui.setupUi(this);

	

	cam.push_back(new ImagingSource());
	cam.back()->serialNumber = "36610014";
	cam.back()->width = 640;
	cam.back()->height = 480;
	cam.back()->depth = 1;
	//cam.back()->offsetX = 300;
	//cam.back()->offsetY = 80;
	cam.back()->FPS = 30;
	cam.back()->trigger = true;
	//cam.back()->listener = true; //za frame ready 
	cam.back()->realTimeProcessing = true; //true -> kliče OnFrameReady v MBCameraView
	cam.back()->triggerOutput = 1;
	//cam.back()->defaultExposure = 1.0 / cameraSettings[0]->parameter[0][2];// dynPar.parameter5[3]; // 1.0 / 5000.0;
	cam.back()->rotadedHorizontal = false;
	cam.back()->rotatedVertical = true;
	//cam.back()->defaultGain = cameraSettings[0]->parameter[0][1];
	cam.back()->num_images = 5;

	QString videoFormat = QString("Y800 (%1x%2)").arg(cam.back()->width).arg(cam.back()->height);
	double expo;
	double gain;
	if (cam.back()->Init(cam.back()->serialNumber, videoFormat) == 1)
		//	if (cam.back()->Init("7710142", "Y800 (1280x960)") == 1)
	{
		if (cam.back()->Start())
		{
		cam.back()->SetExposureAbsolute(1/50);
		expo = 	cam.back()->GetExposureAbsolute();
		cam.back()->SetGainAbsolute(0);
		gain = 	cam.back()->GetGainAbsolute();
		}
	}
	if (!cam.back()->isLive) //ce smo v testnem nacinu in kamera ni odprta, inicializiramo vse slike
		cam.back()->InitImages(640, 480, 1);




	images.push_back(new MemoryBuffer(640, 480, 1));
	images.push_back(new MemoryBuffer(640, 480, 1));
	images.push_back(new MemoryBuffer(640, 480, 1));

	*images[0]->buffer = imread("C:/image/sample.bmp");



	LptControl::OpenDriver();
	lpt.push_back(new LptControl(0x378));
	lpt.push_back(new LptControl(0xD01));


	
	/*QToolButton *lptButtons;

	
	QSignalMapper* signalMapper = new QSignalMapper(this);


	for (int i = 0; i < lpt.size(); ++i)
	{


	lptButtons = new QToolButton(this);
	lptButtons->setText(QString::number(i));
	layout->addWidget(lptButtons);
	connect(lptButtons, SIGNAL(clicked()), signalMapper, SLOT(map()));
	
	signalMapper->setMapping(lptButtons, i);
	
	
	}
	
	connect(signalMapper, SIGNAL(mapped(int)), this, SLOT(onClickedLptButton(int)));

	
	*/


	lpt[0]->SetDataByte(0x00);
	
	lpt[0]->SetControlByte(1, 1);
	lpt[0]->SetControlByte(2, 1);
	lpt[0]->SetControlByte(3, 1);
	lpt[0]->SetControlByte(4, 1);


	//lpt[0]->SetControlByteAllON();
	//serial.push_back( new Serial("COM4",9600));
	
	controlCardMB.push_back( new ControlCardMB("COM14", 115200));
//	controlCardMB.push_back(new ControlCardMB("COM2", 115000));
//
	

	imageProcessingDlg = new imageProcessing();
	imageProcessingDlg->ConnectImages(cam, images);


	//timer za viewDisplay
	OnTimer = new QTimer(this);


	//dinamično dodajanje ikon v tabih
	CreateCameraTab();

	//Povezave SIGNAL-SLOT
	connect(OnTimer, SIGNAL(timeout()), this, SLOT(ViewTimer()));
	OnTimer->start(250);

	for (int i = 0; i < cam.size(); i++)
	{
		connect(cam[i], SIGNAL(frameReadySignal(int, int)), this, SLOT(OnFrameReady(int, int)));
	}

	connect(ui.buttonImageProcessing, SIGNAL(pressed()), this, SLOT(OnClickedShowImageProcessing()));

 ////		MULTIMEDIA	TIMER		////
 UINT uDelay = 20;				// 1 = 1000Hz, 10 = 100Hz
 UINT uResolution = 1;
 DWORD dwUser = NULL;
 UINT fuEvent = TIME_PERIODIC;

 // timeBeginPeriod(1);
 FTimerID = timeSetEvent(uDelay, uResolution, OnMultimediaTimer, dwUser, fuEvent);
}

void MBsoftware32Bit::test()
{


}

void MBsoftware32Bit::OnFrameReady(int cam, int imageIndex)
{
	int bla = 100;


}

void MBsoftware32Bit::OnMultimediaTimer(UINT uTimerID, UINT, DWORD_PTR dwUser, DWORD_PTR dw1, DWORD_PTR dw2)
{

	if (mmTimer.timeCounter >= 1000)
	{
		mmTimer.SetStop();
		mmTimer.ElapsedTime();
		mmTimer.CalcFrequency(mmTimer.timeCounter);
		mmTimer.timeCounter = 0;
		mmTimer.SetStart();
	}

	mmTimer.timeCounter++;

	mmCounter++;
	mmCounter = mmCounter & 0xFFFFFF;




	int bla = 100;


	lpt[0]->GetPortControlValue();
	lpt[0]->GetStatusValue();
	lpt[0]->SetOutputValue(1, 1);

	
	//lpt[0]->SetControlByteAllOFF();

	/*for (int i = 1; i < 5; i++)
	{
		lpt[0]->SetControlByte(i, 1);
}*/
}



void MBsoftware32Bit::CreateCameraTab()
{

	QWidget *cameraWidget;
	QGroupBox *camGroup;
	QHBoxLayout *camLayout = new QHBoxLayout;
	QToolButton *camButton;
	QIcon camIcon;
	
	QSignalMapper* signalMapper = new QSignalMapper(this);
	QWidget *tabWidget;

	QGroupBox *testGroup;

	int k = 0;
	

	camGroup = new QGroupBox( ui.tabCameras);
//	camGroup->setFixedHeight(ui.MainMenuBar->height());
//	camGroup->setFixedWidth(ui.MainMenuBar->width());
	QString fileP =/* QApplication::applicationDirPath() + */QString("D:/Git_MBvision/MBsoftware32bit/MBsoftware32Bit/res/cam%1.bmp").arg(k);
	QImage image(fileP);
	if (!image.isNull())
		camIcon.addPixmap(QPixmap::fromImage(image));

	for (int i = 0; i < 3; i++)
	{
		image.load(QString("D:/Git_MBvision/MBsoftware32bit/MBsoftware32Bit/res/cam%1.bmp").arg(i));
		if (!image.isNull())
			camIcon.addPixmap(QPixmap::fromImage(image));
		camButton = new QToolButton();
		camButton->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
		camButton->setText(QString("CAM%1").arg(i));
		camButton->setFixedSize(QSize(TAB_BUTTTON_SIZE, TAB_BUTTTON_SIZE));
		camButton->setIcon(camIcon);
		camButton->setIconSize(QSize(TAB_ICON_SIZE, TAB_ICON_SIZE));
		camLayout->addWidget(camButton);
		connect(camButton, SIGNAL(clicked()), signalMapper, SLOT(map()));
		signalMapper->setMapping(camButton, i);
	}

	camGroup->setLayout(camLayout);
	connect(signalMapper, SIGNAL(mapped(int)), this, SLOT(OnClickedShowCamButton(int)));





	//	ui.graphicsView->setIcon(testIcon);



	//testIcon.
	//testIconButton->setIcon()


	//testIconButton->setIcon(testIcon);
/*	tabWidget = new QWidget();
	QWidget *newTab = new QWidget(ui.MainMenuBar);
	ui.MainMenuBar->addTab(newTab, tr("name"));
	testGroup = new QGroupBox(tr("test"), ui.tabCameras);
	//ui.tabWidget
	QWidget *window = new QWidget;
	QPushButton *button1 = new QPushButton("One");
	QPushButton *button2 = new QPushButton("Two");
	QPushButton *button3 = new QPushButton("Three");
	QPushButton *button4 = new QPushButton("Four");
	QPushButton *button5 = new QPushButton("Five");


	layout->addWidget(button1);
	layout->addWidget(button2);
	layout->addWidget(button3);
	layout->addWidget(button4);



	*/
	//ui.MainMenuBar->
	//testGroup->setLayout(layout);
	/*

	QWidget * tabWidget;
	QGroupBox * testGroup;
	tabWidget = new QWidget();
	QWidget *newTab = new QWidget(ui.MainMenuBar);
	ui.MainMenuBar->addTab(newTab, tr("name"));
	testGroup = new QGroupBox(tr("test"), newTab);

	QWidget *window = new QWidget;
	QToolButton *button1 = new QToolButton();
	QToolButton *button2 = new QToolButton();
	QToolButton *button3 = new QToolButton();
	QToolButton *button4 = new QToolButton();
	QToolButton *button5 = new QToolButton();

	
	camLayout->addWidget(button1);
	camLayout->addWidget(button2);
	camLayout->addWidget(button3);
	camLayout->addWidget(button4);
	camLayout->addWidget(button5);


	testGroup->setLayout(camLayout);


	
	*/


	/*QToolButton *lptButtons;







	//ui.MainMenuBar->
	testGroup->setLayout(layout);*/
}

void MBsoftware32Bit::CreateSignalsTab()
{

}

void MBsoftware32Bit::paintEvent(QPaintEvent *)
{

	QPainter painter(this);
	QRect rect;
	QLine line;

	rect.setRect(500, 500, 600, 600);
	painter.setPen(Qt::blue);
	painter.setFont(QFont("Arial", 30));


	painter.drawText(rect, Qt::AlignCenter, QString("%1 prijavljen").arg(counter));

 	counter++;
}


void MBsoftware32Bit::ViewTimer()
{
	QByteArray read;

	if (read.size() > 0)
	{

		int bla = 100;
	}
}

void MBsoftware32Bit::OnClickedShowLptButton(int lptNum)
{
	lpt[lptNum]->show();
}
void MBsoftware32Bit::OnClickedShowCamButton(int camNum)
{
	cam[camNum]->ShowDialog();
}
void MBsoftware32Bit::OnClickedShowImageProcessing(void)
{
	imageProcessingDlg->test();

}
void MBsoftware32Bit::keyPressEvent(QKeyEvent *event)
{	
	switch (event->key())
	{
	case  Qt::Key_1:

		break;
	case  Qt::Key_2:
		
		break;

	case  Qt::Key_3:
		

	case  Qt::Key_4:
		
		for (int i = 0; i < cam.size(); i++)
		{
			if (cam[i]->isVisible())
				cam[i]->close();
		}
		break;
	case  Qt::Key_R:
		this->repaint();

		break;
	}
}

