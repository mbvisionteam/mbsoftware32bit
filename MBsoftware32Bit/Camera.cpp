#include "stdafx.h"
#include "Camera.h"

int Camera::objectCounter = 0;
int Camera::cameraOpened = 0;
bool Camera::libraryInitialized = false;

Camera::Camera(QWidget *parent)
	: QWidget(parent)
{
	ui.setupUi(this);
	
	frameCounter = 0;
	prevFrameCounter = 0;
	isLive = 0;
	isLiveOld = 0;
	cameraName = "";
	videoFormat = "";
	vendorName = "";
	FPS = 0;
	rotatedVertical = true;
	rotadedHorizontal = false;
	height = 0;
	width = 0;
	depth = 1;
//	formatType = CV_8UC1;
	widthDisp = 0;
	heightDisp = 0;
	xOffsetDisp = 0;
	yOffsetDisp = 0;
	activeImage = 0;
	isFrameReady = false;
	listener = false;
	trigger = false;
	lockImage = false;
	noFrameCounter = 0;
	grabImage = false;
	imageIndex = 0;
	maxGrabbedImages = 0;
	realTimeProcessing = false;
	grabberInitialized = false;
	triggerTest = false;
	grabSucceeded = true;
	triggerCounter = 0;
	defaultExposure = 0.0001;
	defaultGain = 50;
	id = objectCounter;
	triggerOutput = 0;
	triggIndex = 0;
	objectCounter++;
	currentExposure = 0;
	currentGain = 0;
	enableLive = false;
	isSceneAdded = false;
//	conversionCode = CV_GRAY2RGB;
	num_images = 1; //po defaultu rezervira vsaj eno sliko

	for (int i = 0; i < num_buff; i++)
	{
		//triggerTimer[i].elapsed = -1; //na zacetku so vsi negativni, da se pri triganju loci ze prispele
		encoderValues[i] = 0;
	}

	ClearLockingTable();

	connect(ui.buttonLive, SIGNAL(clicked()), this, SLOT(ShowLive()));
//	connect(ui.btnPrev, SIGNAL(clicked()), this, SLOT(Previous()));
//	connect(ui.btnZoomIn, SIGNAL(clicked()), this, SLOT(ZoomIn()));
//	connect(ui.btnZoomOut, SIGNAL(clicked()), this, SLOT(ZoomOut()));

	connect(this, SIGNAL(frameReadySignal(int, int)), this,SLOT( Live()));

	//DISPLAY
	

	sceneVideo= new QGraphicsScene(this);
	ui.imageView->setScene(sceneVideo); 	
	
	setMouseTracking(true);
	//ui.imageView->viewport()->installEventFilter(this);
	sceneVideo->installEventFilter(this);
	//ui.imageView->viewport()->setMouseTracking(true);
}

Camera::Camera(const Camera& camera)// Copy constructor
{
	frameCounter = camera.frameCounter;
	prevFrameCounter = camera.prevFrameCounter;
	isLive = camera.isLive;
	isLiveOld = camera.isLiveOld;
	cameraName = camera.cameraName;
	videoFormat = camera.videoFormat;
	FPS = camera.FPS;
	rotatedVertical = camera.rotatedVertical;
	rotadedHorizontal = camera.rotadedHorizontal;
	height = camera.height;
	width = camera.width;
	depth = camera.depth;
	activeImage = camera.activeImage;
	vendorName = camera.vendorName;
	isFrameReady = camera.isFrameReady;
	listener = camera.listener;
	trigger = camera.trigger;
	lockImage = camera.lockImage;
	noFrameCounter = camera.noFrameCounter;
	grabImage = camera.grabImage;
	imageIndex = camera.imageIndex;
	maxGrabbedImages = camera.maxGrabbedImages;
	id = objectCounter;
	triggerCounter = camera.triggerCounter;
	realTimeProcessing = camera.realTimeProcessing;
	triggerOutput = camera.triggerOutput;
	widthDisp = camera.widthDisp;
	heightDisp = camera.heightDisp;
	enableLive = camera.enableLive;
	isSceneAdded = camera.isSceneAdded;
	conversionCode = camera.conversionCode;
	objectCounter++;
}


Camera::~Camera()
{
	disconnect(this, 0, 0, 0);
	close();
}

void Camera::ShowLive()
{

	if (enableLive == true)
	{
		ui.buttonLive->setText("Enable Live");
		ui.buttonLive->setStyleSheet("background: rgb(76, 255, 11);");
		DisableLive();
	}
	else
	{
		ui.buttonLive->setText("Disable Live");
		ui.buttonLive->setStyleSheet("background: rgb(255, 59, 33)");
		EnableLive();
		

	}

	ShowImages();
	
		
	



}

void Camera::Live()
{

	
	ShowImages();
}


int Camera::InitImages(int width, int height, int depth)
{


	for (int i = 0; i < num_images; i++)
	{
		if (image.size() != num_images)
		{
			image.push_back(MemoryBuffer(width, height, depth));
			imageReady.push_back(0);
		}
		else
			break;
	}

	return 0;
}
int Camera::InitImages()
{
	return InitImages(width, height, depth);
}

//virtualne metode, redefinirana v CimagingSource, CBasler, CTermo
/*void Camera::DisplayImageRing(CDC *dc, int imageNr, CRect displayRect)
{
}*/
/*void Camera::DisplayLiveImage(CDC *dc, CRect displayRect)
{
}
void Camera::DisplayLiveImage(CDC *dc, CRect displayRect, CRect sourceRect)
{
}

void Camera::DisplayLiveImage(CDC *dc)
{
}
*/
void Camera::CopyImage(int sourceIndex, int destIndex)
{
	/*if ((sourceIndex < image.size()) && (destIndex < image.size()))
	{*/
		/*EDIT	for(int i = 0; i < bufferSize; i++)
		image[destIndex][i] = image[sourceIndex][i];*/
	//}
}
/*
void Camera::DisplayCameraStatus(CDC *dc, CRect displayRect)
{

}*/

void Camera::ClearLockingTable()
{
	int i;
	lockCounter = 0;
	for (i = 0; i < num_buff; i++)
		lockedImagesTable[i] = 0;
}


int Camera::Init(QString serialNumber, QString VideoFormatS)
{
	return 0;
}
int Camera::Init()
{
	return 0;
}

int Camera::Start()
{
	return 0;
}
int Camera::Start(double FPS, bool trigger, bool listener)
{
	return 0;
}
int Camera::Start(double FPS, bool trigger, bool listener, bool flipV, bool flipH)//imagingSource
{
	return 0;
}

int Camera::MemoryUnlock(int imageNr)
{
	return 0;
}
void Camera::MemoryUnlockAll()
{}
int Camera::MemoryLock()
{
	return 0;
}
bool Camera::SetWhiteBalanceAbsolute(long red, long green, long blue)
{
	return true;
}

bool Camera::SetExposureAbsolute(double dExposure)
{
	return false;
}
double Camera::GetExposureAbsolute()
{
	return 0;
}
bool Camera::SetGainAbsolute(double dGain)
{
	return false;
}
int	Camera::GetGainAbsolute()
{
	return 0;
}

int Camera::Init(int selectGrabber, QString cameraName, int width, int height, int depth, int offsetX, int offsetY)
{
	return 0;
}
int Camera::Init(QString cameraName, int width, int height, int depth, int offsetX, int offsetY)
{
	return 0;
}
int Camera::Start(double FPS, int trigger)
{
	return 0;
}
int Camera::Start(double FPS, int trigger, bool flipV, bool flipH)
{
	return 0;
}

void Camera::AutoGainOnce()
{}
void Camera::AutoGainContinuous()
{}
void Camera::AutoExposureOnce()
{}
void Camera::AutoExposureContinuous()
{}
void Camera::AutoWhiteBalance()
{}
bool Camera::IsColorCamera()
{
	return false;
}
int Camera::GrabImage(int imageNr)
{
	return 0;
}
void Camera::CloseGrabber(void)
{
}
bool Camera::OnSettingsImage()
{
	return false;
}
bool Camera::SaveReferenceSettings()
{
	return false;
}
bool Camera::LoadReferenceSettings()
{
	return false;
}
bool Camera::IsDeviceValid()
{
	return false;
}
/*
void Camera::DisplayData(CDC *pDC, CRect displayRec)
{
}*/

bool Camera::EnableLive()
{
	return false;
}

bool Camera::DisableLive()
{
	return false;
}

void Camera::ShowDialog()
{
	ResizeDisplayRect();
	setWindowModality(Qt::ApplicationModal);
	show();
	ShowImages();
}

void Camera::ResizeDisplayRect()
{
//	if ((ui.imageView->width() != width) || (ui.imageView->height() != height))
	{

		if ((width != 0) && (height != 0))
		{
			//resize(width + 10, height + 90);

			//ui.imageView->resize(width + 10, height + 10);
			ui.imageView->setGeometry(QRect(0, 0, width, height));
			ui.imageView->setAlignment(Qt::AlignTop | Qt::AlignLeft);
			ui.imageView->setSceneRect(QRectF(0, 0, width, height));


			//za zoom: ui.imageView->fitInView(QRectF(0, 0, image[activeImage].cols+ 10, image[activeImage].rows + 10), Qt::IgnoreAspectRatio);
		}
	}
}

int Camera::ConvertImageForDisplay(int imageNumber)
{
	
	Mat test;
	if ((image.size() > imageNumber) && (width != 0) && (height != 0))
	{
		cv::cvtColor(image[imageNumber].buffer[0], test, conversionCode);
		drawPicture = test(Rect(0, 0,640,480 ));

		return 1;
	}

	return 0;
}

void Camera::ShowImages()
{
	if (ConvertImageForDisplay(0))
	{
	QImage qimgOriginal((uchar*)drawPicture.data, drawPicture.cols, drawPicture.rows, drawPicture.step, QImage::Format_RGB888);

		if (!isSceneAdded)
		{
			//DISPLAY
			//sceneVideo = new QGraphicsScene(this);
			//ui.imageView->setScene(sceneVideo);
			pixmapVideo = sceneVideo->addPixmap(QPixmap::fromImage(qimgOriginal));
			
			isSceneAdded = true;
		}
	
		pixmapVideo->setPixmap(QPixmap::fromImage(qimgOriginal));

		//ui.labelCamera->setText(QString("%1").arg(cameraName));
	//	ui.labelPixelFormat->setText(QString("%1").arg(videoFormat));
	//	ui.labelImageNumber->setText(QString("Image number: %1").arg(activeImage));
	}
}
void Camera::SaveImage(int imageindex, int id)
{
	/*if (image.size() > imageindex)
	{
		string file = format("../RefImages/%d.bmp", id);
		imwrite(file, image[imageindex]);
	}*/
}


void Camera::SetPacketSize(int size)
{
}


void Camera::Next()
{
	activeImage++;
	/*if (activeImage > image.size())
		activeImage = 0;*/

	ShowImages();
}
void Camera::Previous()
{
	activeImage--;
	if (activeImage < 0)
		//activeImage = image.size() - 1;

	ShowImages();

}
void Camera::ZoomIn()
{

}
void Camera::ZoomOut()
{
}



bool Camera::eventFilter(QObject *obj, QEvent *event)
{
	uchar b = 0, g = 0, r = 0;
	QPoint point_mouse;
	QGraphicsLineItem * item;
	QPointF position;
	int x, y;

	if (event->type() == QEvent::GraphicsSceneMousePress)
	{
		const QGraphicsSceneMouseEvent* const me = static_cast<const QGraphicsSceneMouseEvent*>(event);
		const QPointF position = me->scenePos();
		int bla = 100;

		bla = 100;
	}

	if (event->type() == QEvent::GraphicsSceneMouseMove)
	{
		const QGraphicsSceneMouseEvent* const me = static_cast<const QGraphicsSceneMouseEvent*>(event);
		position = me->scenePos();

		x = (int)position.x();
		y = (int)position.y();

		if ((x < width) && (y < height) && (x > -1) && (y > -1))
		{
			if (depth == 1)
			{
				b = g = r = image[0].buffer[0].data[image[0].buffer[0].channels()*(image[0].buffer[0].cols*y + x)];
			}
			else
			{
				b = image[0].buffer[0].data[image[0].buffer[0].channels()*(image[0].buffer[0].cols*y + x) + 0];
				g = image[0].buffer[0].data[image[0].buffer[0].channels()*(image[0].buffer[0].cols*y + x) + 1];
				r = image[0].buffer[0].data[image[0].buffer[0].channels()*(image[0].buffer[0].cols*y + x) + 2];
			}
				

		ui.labelG->setText(QString("X = %1").arg(x));
		ui.labelB->setText(QString("Y = %1").arg(y));
		ui.lableR->setText(QString("R = %1 ").arg(r));


		}


	}




		/*if (event->type() == QEvent::MouseMove)
		{
			int x, y;
			
			item = new QGraphicsLineItem;
			QGraphicsSceneMouseEvent  *GraphEvent = static_cast<QGraphicsSceneMouseEvent*>(event);
			QMouseEvent  *mouseEvent = static_cast<QMouseEvent*>(event);
			point_mouse = ui.imageView->mapFrom(ui.imageView, mouseEvent->pos());
			QPointF point;
		///	point =GraphEvent->scenePos();

			
			return true;
		}*/
	

	return true;
}

/*void Camera::mouseMoveEvent(QGraphicsSceneMouseEvent * event)
{


	int bla = 100;

	bla = 100;
}

void Camera::mousePressEvent(QGraphicsSceneMouseEvent * event)
{
	int bla = 100;

	bla = 1;

}*/