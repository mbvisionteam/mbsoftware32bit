#pragma once
#include <QSerialPort>
#include <QSerialPortInfo>
#include <qwidget.h>
#include "ui_Serial.h"

class Serial : public QWidget
{
	Q_OBJECT
public:
	Serial();

	Serial(QString port, int baudRate);


	virtual ~Serial();




private:
	Ui::Serial ui;


protected:


public:

	QSerialPort *serial;





public slots:
	void ReadData();
};

