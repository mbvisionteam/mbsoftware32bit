#include "imageProcessing.h"
std::vector<Camera*>						imageProcessing::processCam;
std::vector<MemoryBuffer*>					imageProcessing::processImages;
MemoryBuffer*								imageProcessing::currImage;
MemoryBuffer								imageProcessing::DrawImage;





imageProcessing::imageProcessing(QWidget *parent)
	: QWidget(parent)
{
	ui.setupUi(this);
	zoomFactor = 0;
	displayImage = 0;
	displayWindow = 0;
	prevDisplayImage = 0;
	prevDisplayWindow = 0;
	isSceneAdded = false;

	scene = new QGraphicsScene(this);
	ui.imageView->setScene(scene);
	setMouseTracking(true);
	scene->installEventFilter(this);
	
	connect(ui.buttonImageUp, SIGNAL(pressed()), this, SLOT(OnPressedImageUp()));
	connect(ui.buttonImageDown, SIGNAL(pressed()), this, SLOT(OnPressedImageDown()));
	connect(ui.buttonCamDown, SIGNAL(pressed()), this, SLOT(OnPressedCamDown()));
	connect(ui.buttonCamUp, SIGNAL(pressed()), this, SLOT(OnPressedCamUp()));

	
}

/*imageProcessing::imageProcessing()
{
}*/





imageProcessing::~imageProcessing()
{


}

void imageProcessing::ConnectImages(std::vector<Camera*> inputCam, std::vector<MemoryBuffer*> inputImages)
{

	processCam = inputCam;
	processImages = inputImages;
	currImage = &processCam[0]->image[0];
	displayWindow = 1;
	ReplaceCurrentBuffer();
	
}

void imageProcessing::ConnectImages(std::vector<Camera*> inputCam)
{
	processCam = inputCam;
	processCam[0]->imageReady[0] = 1000;
	ReplaceCurrentBuffer();

}

void imageProcessing::test()
{
	processCam[0];

	QFileDialog dialog;// uporabljen za load images from file


	show();


}

bool imageProcessing::eventFilter(QObject *obj, QEvent *event)
{
	uchar b = 0, g = 0, r = 0;
	QPoint point_mouse;
	QGraphicsLineItem * item;
	QPointF position;
	int x, y;

	if (event->type() == QEvent::GraphicsSceneMousePress)
	{
		const QGraphicsSceneMouseEvent* const me = static_cast<const QGraphicsSceneMouseEvent*>(event);
		const QPointF position = me->scenePos();
		int bla = 100;

		bla = 100;
	}

	if (event->type() == QEvent::GraphicsSceneMouseMove)
	{
		const QGraphicsSceneMouseEvent* const me = static_cast<const QGraphicsSceneMouseEvent*>(event);
		position = me->scenePos();
		x = (int)position.x();
		y = (int)position.y();

		/*if ((x < width) && (y < height) && (x > -1) && (y > -1))
		{
			if (depth == 1)
			{
						b = g = r = image[0].data[image[0].channels()*(image[0].cols*y + x) + 0];
					}
					else
					{
						b = image[0].data[image[0].channels()*(image[0].cols*y + x) + 0];
						g = image[0].data[image[0].channels()*(image[0].cols*y + x) + 1];
						r = image[0].data[image[0].channels()*(image[0].cols*y + x) + 2];
					}
				}

				ui.labelG->setText(QString("X = %1").arg(x));
				ui.labelB->setText(QString("Y = %1").arg(y));
				ui.lableR->setText(QString("R = %1 ").arg(r));



			}*/
	}
	return true;
}

void imageProcessing::mousePressEvent(QMouseEvent * event)
{
	/*if (event->buttons() & Qt::LeftButton)
	{
		
	}*/
}


void imageProcessing::ReplaceCurrentBuffer()
{
	if ((displayImage != prevDisplayImage) || (displayWindow != prevDisplayWindow))
	{
		if (displayWindow == processCam.size())
		{
			currImage = processImages[displayImage];
		}
		else
		{
			currImage = &processCam[displayWindow]->image[displayImage];
		}

		prevDisplayImage = displayImage;
		prevDisplayWindow = displayWindow;
	}
	
	if (displayWindow == processCam.size())
	{
		ui.labelLocation->setText(QString("SAVED IMAGES: %1").arg(displayImage));
	}
	else
		ui.labelLocation->setText(QString("CAM: %1, IMAGE: %2").arg(displayWindow).arg(displayImage));

	//ShowImages();

}



void imageProcessing::OnPressedImageUp()
{
	displayImage++;
	if (displayWindow == processCam.size())
	{
		if (displayImage > processImages.size()-1)
			displayImage = 0;
	}
	else
	{
		if (displayImage > processCam[displayWindow]->num_images-1)
			displayImage = 0;

	}
	ReplaceCurrentBuffer();

}

void imageProcessing::OnPressedImageDown()
{
	displayImage--;
	if (displayWindow == processCam.size())
	{
		if ((displayImage > processImages.size()) ||(displayImage < 0))
			displayImage = processImages.size()-1;
	}
	else
	{
		if((displayImage >= processCam[displayWindow]->num_images-1) || (displayImage < 0))
			displayImage = processCam[displayWindow]->num_images - 1;

	}

	ReplaceCurrentBuffer();
}

void imageProcessing::OnPressedCamUp()
{
	displayWindow++;
	if ((displayWindow < 0) || (displayWindow > processCam.size()))
		displayWindow =0;
	ReplaceCurrentBuffer();
}

void imageProcessing::OnPressedCamDown()
{
	displayWindow--;
	if ((displayWindow < 0) || (displayWindow > processCam.size()))
		displayWindow = processCam.size() ;
	ReplaceCurrentBuffer();
}





int imageProcessing::ConvertImageForDisplay(int imageNumber)
{
	Mat test;

	

		cvtColor(*currImage->buffer, test, CV_BGR2RGB);
	//	DrawImage.buffer = test(Rect(0, 0, currImage->width, currImage->height));

		return 1;

	

}

void imageProcessing::ShowImages()
{
	if (ConvertImageForDisplay(0))
	{
		QImage qimgOriginal((uchar*)DrawImage.buffer->data, DrawImage.buffer->cols, DrawImage.buffer->rows, DrawImage.buffer->step, QImage::Format_RGB888);

		if (!isSceneAdded)
		{
			//DISPLAY
			//sceneVideo = new QGraphicsScene(this);
			//ui.imageView->setScene(sceneVideo);
			pixmapVideo = scene->addPixmap(QPixmap::fromImage(qimgOriginal));

			isSceneAdded = true;
		}

		pixmapVideo->setPixmap(QPixmap::fromImage(qimgOriginal));

		//ui.labelCamera->setText(QString("%1").arg(cameraName));
	//	ui.labelPixelFormat->setText(QString("%1").arg(videoFormat));
	//	ui.labelImageNumber->setText(QString("Image number: %1").arg(activeImage));
	}
}

	/*if (event->type() == QEvent::MouseMove)
	{
		int x, y;

		item = new QGraphicsLineItem;
		QGraphicsSceneMouseEvent  *GraphEvent = static_cast<QGraphicsSceneMouseEvent*>(event);
		QMouseEvent  *mouseEvent = static_cast<QMouseEvent*>(event);
		point_mouse = ui.imageView->mapFrom(ui.imageView, mouseEvent->pos());
		QPointF point;
	///	point =GraphEvent->scenePos();


		return true;
	}*/

