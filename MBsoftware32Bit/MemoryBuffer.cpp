#include "MemoryBuffer.h"



MemoryBuffer::MemoryBuffer()
{
}

MemoryBuffer::MemoryBuffer(int width, int height, int depth)
{
	this->width = width;
	this->depth = depth;
	this->height = height;
	Create(width, height, depth);
}


MemoryBuffer::~MemoryBuffer()
{
}

void MemoryBuffer::Create(int width, int height, int depth)
{
	
	if (depth == 1)
		buffer = new Mat(height, width, CV_8UC1, 1);
	else if (depth == 2)
		buffer = new Mat(height, width, CV_8UC1, 2);
	else if (depth == 3)
		buffer = new Mat(height, width, CV_8UC1, 3);
	else if (depth == 4)
		buffer = new Mat(height, width, CV_8UC1, 4);
		

	/*if (format.compare("Y800") == 0)
	{
		buffer = new Mat(height, width, CV_8UC1);
		//formatType = CV_8UC1;
	}
	if (format.compare("Y16") == 0)
	{
		buffer = new Mat(height, width, CV_16UC1);
		
	}
	if (format.compare("RGB24") == 0)
	{
		buffer = new Mat(height, width, CV_8UC1,3);


	}

	if (format.compare("RGB32") == 0)
	{
		buffer = new Mat(height, width, CV_8UC1,4);
	}
	*/

}
